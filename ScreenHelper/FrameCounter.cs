using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace Toucher
{
    public class FrameCounter
    {
        SpriteFont spriteFont;
        private const int ROUND = 2;
        private float deltaTime;

        private long totalFrames;
        public long TotalFrames
        {
            get
            {
                return (long)Math.Round((decimal)totalFrames, ROUND, MidpointRounding.AwayFromZero);
            }
        }

        private float totalSeconds;
        public float TotalSeconds
        {
            get
            {
                return (float)Math.Round((decimal)totalSeconds, ROUND, MidpointRounding.AwayFromZero);
            }
        }

        private float averageFPS;
        public float AverageFPS
        {
            get
            {
                return (float)Math.Round((decimal)averageFPS, ROUND, MidpointRounding.AwayFromZero);
            }
         }

        private float currentFPS;
        public float CurrentFPS
        {
            get
            {
                return (float)Math.Round((decimal)currentFPS, ROUND, MidpointRounding.AwayFromZero);
            }
         }

        public int maximum_samples;

        private Queue<float> sampleBuffer;

        public FrameCounter(int maximum_samples = 100)
        {
            this.maximum_samples = maximum_samples;
            sampleBuffer = new Queue<float>();
        }

        public void Load(SpriteFont spriteFont)
        {
            int aa = 1;
            this.spriteFont = spriteFont;
        }

        public void Update(GameTime gameTime)
        {
            //Time for last frame
            deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            currentFPS = 1.0f / deltaTime;
            sampleBuffer.Enqueue(CurrentFPS);

            if(sampleBuffer.Count > maximum_samples)
            {
                sampleBuffer.Dequeue();
                averageFPS = sampleBuffer.Average(i => i);
            }
            else
            {
                averageFPS = CurrentFPS;
            }

            totalFrames++;
            totalSeconds += deltaTime;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(spriteFont, $"FPS: {AverageFPS}; Time: {TotalSeconds}", new Vector2(2, 2), Color.Black);
        }
    }
}